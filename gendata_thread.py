from sql_v2 import *
from connect import *
import threading
import time
import urllib.request

CONST = 60
user_id = 1

class GenerateDataThread(threading.Thread):
    def run(self):
        host = 'generate-data-server-2.react-master.svc.cluster.local'
        port = '7000'
        url = 'http://{0}:{1}/generateData'.format(host, port)
        while True:
            with urllib.request.urlopen(url) as f:
                result = f.read().decode('utf-8')
            host = 'mysql.react-master.svc.cluster.local'
            user = 'testdb'
            pw = 'testdb'
            database = 'testdb'
            connect = Connect(host, user, pw, database)
            answer = insert_deals_to_db(connect, result, user_id)
            time.sleep(CONST)