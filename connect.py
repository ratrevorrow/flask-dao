class Connect:
    def __init__(self, host, user, pw, database, port=3306):
        self.__host = host
        self.__user = user
        self.__pw = pw
        self.__database = database
        self.__port = port

    def get_host(self):
        return self.__host

    def get_user(self):
        return self.__user

    def get_pw(self):
        return self.__pw

    def get_database(self):
        return self.__database

    def get_port(self):
        return self.__port
