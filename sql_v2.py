import mysql.connector
import datetime
import json


def insert_deals_to_db(connect, deals_to_add_json, tr_id=1):
    cnx = mysql.connector.connect(user=connect.get_user(), passwd=connect.get_pw(),
                                  database=connect.get_database(), host=connect.get_host(),
                                  port=connect.get_port())
    cursor = cnx.cursor()

    add_deal = ("INSERT INTO Deal (quantity, d_time, P_ID, U_ID, CP_ID, I_ID) VALUES (%s, %s, %s, %s, %s, %s)")

    add_price = ("INSERT INTO Price (P_ID, tpe, price) VALUES (%s, %s, %s)")

    query = ("SELECT MAX(P_ID) as MMAX FROM Price")
    cursor.execute(query)
    mmax = 0
    for MMAX in cursor:
        if MMAX[0] is not None:
            mmax = MMAX[0]
    cursor.close()
    cursor = cnx.cursor()

    deals_to_add = json.loads(deals_to_add_json)

    data_deal = []
    data_price = []
    for j in deals_to_add:
        query = ("SELECT I_ID FROM Instruments WHERE Iname = %s")
        i = json.loads(j)
        cursor.execute(query, (i["instrumentName"], ))
        i_id = (1,)
        for I_ID in cursor:
            i_id = I_ID
        cursor.close()
        cursor = cnx.cursor()

        query = "SELECT CP_ID FROM Counterparty WHERE name = %s"
        cursor.execute(query, (i['cpty'], ))
        cp_id = (1,)
        for CP_ID in cursor:
            cp_id = CP_ID
        cursor.close()
        cursor = cnx.cursor()

        dt = datetime.datetime.strptime(i['time'][:21] + ')', "%d-%b-%Y (%H:%M:%S)")
        if tr_id != cp_id:
            mmax += 1
            data_deal.append((i['quantity'], dt, mmax, tr_id, cp_id[0], i_id[0]))
            data_price.append((mmax, i['type'], i['price']))


    cursor.executemany(add_price, data_price)
    cnx.commit()
    cursor.close()

    cursor = cnx.cursor()
    cursor.executemany(add_deal, data_deal)
    cnx.commit()
    cursor.close()
    cnx.close()

    return deals_to_add


def select_deals_from_db(connect, tr_id=1):
    cnx = mysql.connector.connect(user=connect.get_user(), passwd=connect.get_pw(),
                                  database=connect.get_database(), host=connect.get_host(),
                                  port=connect.get_port())
    cursor = cnx.cursor()

    deals_to_return = []

    query = ("SELECT quantity, price, tpe, Iname FROM Deal"
             " JOIN (SELECT price, tpe, P_ID FROM Price) p on Deal.P_ID = p.P_ID"
             " JOIN (SELECT Iname, I_ID FROM Instruments) i on Deal.I_ID = i.I_ID"
             " WHERE U_ID = %s")

    cursor.execute(query, (str(tr_id), ))
    for (quantity, price, tpe, Iname) in cursor:
        tmp_dict = {'quantity': quantity, 'price': price, 'type': tpe, 'instrumentName': Iname}
        deals_to_return.append(json.dumps(tmp_dict))

    cursor.close()
    cnx.close()

    return deals_to_return


def select_user_info(connect, user_name):
    cnx = mysql.connector.connect(user=connect.get_user(), passwd=connect.get_pw(),
                                  database=connect.get_database(), host=connect.get_host(),
                                  port=connect.get_port())
    cursor = cnx.cursor()

    query = ("SELECT U_ID, user_pw, tpe FROM Users WHERE user_name = %s")

    cursor.execute(query, (user_name, ))

    U_ID = 1
    tmp_dict = {'U_ID': U_ID, 'user_pw': '', 'tpe': 'T'}
    for (U_ID, user_pw, tpe) in cursor:
        tmp_dict = {'U_ID': U_ID, 'user_pw': user_pw, 'tpe': tpe}

    cursor.close()
    cnx.close()

    return json.dumps(tmp_dict), U_ID
