from flask import Flask, jsonify, request
from flask_cors import CORS
from sql_v2 import *
from connect import *
import urllib.request
from gendata_thread import *

app = Flask(__name__)
CORS(app)

TIME_PERIOD_MILLIS = 3600000

user_id = 1


def generate_data(json_list=False):
    host = 'generate-data-server-2.react-master.svc.cluster.local'
    port = '7000'
    url = 'http://{0}:{1}/generateData'.format(host, port)
    with urllib.request.urlopen(url) as f:
        result = f.read().decode('utf-8')
    if json_list:
        return result
    return jsonify(result)


def insert_data():
    result = generate_data(True)
    host = 'mysql.react-master.svc.cluster.local'
    user = 'testdb'
    pw = 'testdb'
    database = 'testdb'
    connect = Connect(host, user, pw, database)
    answer = insert_deals_to_db(connect, result, user_id)
    return jsonify(answer)


@app.route("/selectData")
def select_data():
    host = 'mysql.react-master.svc.cluster.local'
    user = 'testdb'
    pw = 'testdb'
    database = 'testdb'
    connect = Connect(host, user, pw, database)
    answer = select_deals_from_db(connect, user_id)
    return jsonify(answer)


@app.route("/selectUser")
def select_user():
    user_name = request.args.get('name')
    host = 'mysql.react-master.svc.cluster.local'
    user = 'testdb'
    pw = 'testdb'
    database = 'testdb'
    connect = Connect(host, user, pw, database)
    answer, u_id = select_user_info(connect, user_name)
    global user_id
    user_id = u_id
    return jsonify(answer)


def bootapp():
    gendata_thread = GenerateDataThread()
    gendata_thread.start()
    app.run(port=8080, threaded=True, host=('0.0.0.0'))


if __name__ == '__main__':
     bootapp()