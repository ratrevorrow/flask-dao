import unittest
from sql_v2 import *
from connect import *
import json


class TestSQL(unittest.TestCase):
    def testSelectUser(self):
        host = '192.168.99.100'
        user = 'root'
        pw = 'root'
        database = 'mydb2'
        connect = Connect(host, user, pw, database)
        user_name = 'Tradertest'
        correct_id = 1
        answer, u_id = select_user_info(connect, user_name)
        assert u_id == correct_id, "The U_ID (user_id) should be " + str(correct_id) + " not " + str(u_id)
        assert isinstance(answer, str), "The result should be string, not " + str(type(answer))
        ans = json.loads(answer)
        assert isinstance(ans, dict), "This variable should be a dict, not a " + str(type(ans))
        assert ans.keys() == set(['U_ID', 'user_pw', 'tpe']), "Incorrect set of dict keys"
        assert isinstance(ans['U_ID'], int), "The user_id (U_ID) should be int, not " + str(type(ans['U_ID']))
        assert isinstance(ans['user_pw'], str), "The password (user_pw) should be string, not " + str(type(ans['user_pw']))
        assert isinstance(ans['tpe'], str) and len(ans['tpe']) == 1,\
            "The type (tpe) should be string with length 1, not " + str(type(ans['tpe'])) \
            + " with length " + str(len(ans['tpe']))

    def testInsertListOfDeal_select(self):
        host = '192.168.99.100'
        user = 'root'
        pw = 'root'
        database = 'mydb2'
        connect = Connect(host, user, pw, database)
        deals_to_add_json = []
        num = 10
        for i in range(num):
            deals_to_add_json.append(json.dumps({"instrumentName": "Galactia",
                                                 "cpty": "Lewis",
                                                 "price": i,
                                                 "type": "S",
                                                 "quantity": i,
                                                 "time": "11-Aug-2019 (12:07:06.471252)"}))

        cnx = mysql.connector.connect(user=connect.get_user(), passwd=connect.get_pw(),
                                      database=connect.get_database(), host=connect.get_host(),
                                      port=connect.get_port())
        cursor = cnx.cursor()
        query = ("INSERT INTO Users (U_ID, user_name, user_pw, tpe) VALUES (%s, %s, %s, %s)")
        uuser = (10, 'DB', '', 'T')
        cursor.executemany(query, [uuser])
        cnx.commit()
        cursor.close()
        cnx.close()

        insert_deals_to_db(connect, json.dumps(deals_to_add_json), 10)
        return_deals = select_deals_from_db(connect, 10)
        assert isinstance(return_deals, list), "The result should be list, not " + str(type(return_deals))
        assert len(return_deals) == num, "The result list should have length 10, not " + str(len(return_deals))
        for i, j in enumerate(return_deals):
            assert isinstance(j, str), "Each element of result should be string, not, " + str(type(j))
            res = json.loads(j)
            cor = json.loads(deals_to_add_json[i])
            assert res.keys() == set(['quantity', 'price', 'type', 'instrumentName']), "Incorrect set of keys"
            for k in res.keys():
                assert res[k] == cor[k], "The result data should be the same as inserteted one"

        cnx = mysql.connector.connect(user=connect.get_user(), passwd=connect.get_pw(),
                                      database=connect.get_database(), host=connect.get_host(),
                                      port=connect.get_port())
        cursor = cnx.cursor()
        query = ("DELETE FROM Deal WHERE U_ID = %s")
        args = [(10,)]
        cursor.executemany(query, args)
        cnx.commit()
        cursor.close()

        cursor = cnx.cursor()
        query = ("DELETE FROM Users WHERE U_ID = %s")
        cursor.executemany(query, args)
        cursor.close()
        cnx.commit()
        cnx.close()


if __name__ == '__main__':
    unittest.main()

